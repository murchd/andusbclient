package nz.co.wetstone;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class USBClientActivity extends Activity implements OnClickListener {
	public static final String TAG = "Connection";
	public static final int TIMEOUT = 10;
	Intent i = null;
	TextView tv = null;
	private String connectionStatus = null;
	private Handler mHandler = null;
	ServerSocket server = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.connection);

		// Set up click listeners for the buttons
		View connectButton = findViewById(R.id.btnConnect);
		connectButton.setOnClickListener(this);

		i = new Intent(this, Connected.class);
		mHandler = new Handler();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnConnect:
			tv = (TextView) findViewById(R.id.tvConnection);
			// Initialise server socket in a new separate thread
			new Thread(initializeConnection).start();
			String msg = "Attempting to connect…";
			Toast.makeText(USBClientActivity.this, msg, msg.length()).show();
			break;
		}
	}

	private Runnable initializeConnection = new Thread() {
		private boolean connected;

		public void run() {

			Socket client = null;
			// Initialise server socket
			try {
				server = new ServerSocket(38300);
				//server.setSoTimeout(TIMEOUT * 1000);
				Log.d(TAG, "waiting for connection");
				while(true) {
					client = server.accept();
					try {
						BufferedReader socketIn = new BufferedReader(
								new InputStreamReader(client.getInputStream()));
						String line = null;
						PrintWriter socketOut = new PrintWriter(client.getOutputStream(), true);
						while ((line = socketIn.readLine()) != null) {
							// read here
							System.out.println(">>>>> "+line);
						}
						// break;
					} catch (Exception e) {
						Log.d(TAG, "lost client");
						e.printStackTrace();
					}
					sleep(200);
				}
			} catch (Exception e) {

				connectionStatus = "Disconnected";
				mHandler.post(showConnectionStatus);
			} finally {
				// close the server socket
				try {
					if (server != null)
						server.close();
				} catch (IOException ec) {
					Log.e(TAG, "Cannot close server socket" + ec);
				}
			}

			if (client != null) {
				connected = true;
				// print out success
				connectionStatus = "Connection was succesful!";
				mHandler.post(showConnectionStatus);

				startActivity(i);
			}
		}
	};

	/**
	 * Pops up a “toast” to indicate the connection status
	 */
	private Runnable showConnectionStatus = new Runnable() {
		public void run() {
			Toast.makeText(USBClientActivity.this, connectionStatus,
			Toast.LENGTH_SHORT).show();
		}
	};
}